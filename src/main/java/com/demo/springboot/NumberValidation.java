package com.demo.springboot;

import org.springframework.stereotype.Service;

@Service
public class NumberValidation {

    private final int LENGTH = 11;
    private final byte[] PESEL = new byte[11];

    public boolean isValid(String pesel) {

        if (pesel.length() != LENGTH) {
            return false;
        } else {
            for (int i = 0; i < 11; i++) {
                PESEL[i] = Byte.parseByte(pesel.substring(i, i + 1));
            }
            return countCheck();
        }
    }


    private boolean countCheck() {

        int temp = PESEL[0]+
                3 * PESEL[1]+
                7 * PESEL[2]+
                9 * PESEL[3]+
                PESEL[4]+
                3 * PESEL[5]+
                7 * PESEL[6]+
                9 * PESEL[7]+
                PESEL[8]+
                3 * PESEL[9];
        temp %= 10;
        temp = 10 - temp;
        temp %= 10;

        return temp == PESEL[10];

    }

}


