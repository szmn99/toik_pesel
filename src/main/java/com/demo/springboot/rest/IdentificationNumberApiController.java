package com.demo.springboot.rest;

import com.demo.springboot.NumberValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class IdentificationNumberApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IdentificationNumberApiController.class);
    NumberValidation numberValidation = new NumberValidation();

    @CrossOrigin
    @GetMapping(value = "/check-identification-number")
    public ResponseEntity<Void> checkIdentificationNumber(@RequestParam(defaultValue = "") String id) {

        LOGGER.info("--- check identification number: {}", id);
        if(numberValidation.isValid(id)){
            LOGGER.info("Valid ID");
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else{
            LOGGER.info("Invalid ID");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

}
